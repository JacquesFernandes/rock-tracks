import React, { Component } from 'react';
import {
  Card
} from 'semantic-ui-react';

class CustomHeader extends Component {

  render() {
    return(
      <Card fluid >
        <Card.Content>
          <Card.Header>
            Rock Tracks
          </Card.Header>
        </Card.Content>
      </Card>
    );
  }
}

export default CustomHeader;