import React from 'react';
import { 
  Image,
} from 'semantic-ui-react';

class Artwork extends React.Component {

  render() {
    return(
      <Image
        src={this.props.url}
        style={{cursor:"pointer"}}
        onClick={() => {
          window.open(this.props.url);
        }}
      />
    );
  }

}

export default Artwork;