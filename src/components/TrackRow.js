import React from 'react';
import {
  Table,
  Button,
} from 'semantic-ui-react';
import {
  Link
} from 'react-router-dom';

import Artwork from './Artwork.js';

// Clicking on the artwork will open the artwork in a new tab
// Comment out the <Artwork> and un-comment the Button if you want to stick to "click link to open in new tab" condition

function generateTrackRow({ trackName, artistName, trackPrice, currency, artworkUrl100, trackId }, index) {
  
  return(
    <Table.Row key={"tr_"+index} >
      <Table.Cell>{ trackName }</Table.Cell>
      <Table.Cell>{ artistName }</Table.Cell>
      <Table.Cell>{currency} { trackPrice }</Table.Cell>
      <Table.Cell>
        {/* <Button 
          label="artwork"
          icon="external"
          onClick={() => {
            window.open(artworkUrl100);
          }}
        /> */}
        <Artwork url={artworkUrl100} />
      </Table.Cell>
      <Table.Cell>
        <Link to={"/track/"+trackId}>
          <Button
            label="Details"
            icon="external"
            primary
          />
        </Link>
      </Table.Cell>
    </Table.Row>
  );
}

export default generateTrackRow;