import React, { Component } from 'react';
import _ from 'lodash';
import axios from 'axios';
import {
  Card,
  Item,
  Message,
  Button,
} from 'semantic-ui-react';

import { api_url } from '../constants.js';
import Artwork from '../components/Artwork.js';

class Details extends Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      trackId: null,
      track: null,
    }
  }

  componentDidMount() {
    axios.get(api_url)
    .then(({ data }) => {
      var { trackId } = this.props.match.params;

      trackId = /^[0-9]+$/.test(trackId)? Number(trackId) : null; // check if trackId is a number and convert
      var track = _.find(data.results, { trackId });

      this.setState({
        track,
        trackId,
        loading: false,
      });
    })
    .catch((err) => {
      console.error("ERROR:",err);
    });
  }

  generateDetail(labelName, data) { // do away with repeatedly having to create these items
    return(
      <Item>
        <Item.Header>
          {labelName}
        </Item.Header>
        <Item.Meta>
          {data}
        </Item.Meta>
      </Item>
    )
  }

  render() {

    if(!this.state.loading && this.state.track){ // data has loaded and track was found

      var { track } = this.state;
      var trackTime = new Date(track.trackTimeMillis);
      
      var releaseDate = new Date(track.releaseDate);
      releaseDate = { // create a simple format-ready object for use
        ...releaseDate,
        dd: (releaseDate.getDate() < 10)? "0"+releaseDate.getDate() : releaseDate.getDate(),
        mm: (releaseDate.getMonth() + 1 < 10)? "0"+(releaseDate.getMonth()+1) : releaseDate.getMonth()+1,
        yyyy: releaseDate.getUTCFullYear()
      };

      return(
        <Card fluid >
          <Card.Content>
            <Artwork url={track.artworkUrl100} />
          </Card.Content>
          <Card.Content>
            <h4>
              {this.generateDetail("Track Name", track.trackName)} <br/>
              {this.generateDetail("Artist", track.artistName)} <br/>
              {this.generateDetail("Duration", <div>{trackTime.getMinutes()}:{trackTime.getSeconds()}</div>)} <br/>
              {this.generateDetail("Release Date (dd-mm-yyyy)", <div>{releaseDate.dd}-{releaseDate.mm}-{releaseDate.yyyy}</div>)} <br/>
              {this.generateDetail("Track Price", <div>{track.currency} {track.trackPrice}</div>)} <br/>
            </h4>
            <Button 
              label="More Details"
              icon="external"
              primary
              onClick={() => {
                window.open(track.trackViewUrl);
              }}
            />
          </Card.Content>
        </Card>
      );
    }
    else if(this.state.loading) { // while data is being fetched in componentDidMount
      return(
        <Message 
          content="loading..."
          positive
        />
      );
    }
    else{ // in case no track-data is found
      return(
        <Card fluid >
          <Card.Content>
            <Card.Header>
              <Message 
                content={"No Track found for this id: "+this.state.trackId}
                error
              />
            </Card.Header>
          </Card.Content>
        </Card>
      )
    }
  }

}

export default Details;