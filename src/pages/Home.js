import React, { Component } from 'react';
import axios from 'axios';
import {
  Card,
  Table,
} from 'semantic-ui-react';

import { api_url } from '../constants.js'
import generateTrackRow from '../components/TrackRow.js';

class Home extends Component {

  constructor(props) {
    super(props);

    this.state = {
      results: [],
    }
  }

  componentDidMount() {
    axios.get(api_url)
    .then(({ data }) => {
      var { results } = data;
      this.setState({ results })
    })
    .catch((err) => {
      console.error("ERROR:",err);
    });
  }

  render() {
    return(
      <Card fluid >
        <Card.Content>
          <Table>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell content="Track Name" />
                <Table.HeaderCell content="Artist" />
                <Table.HeaderCell content="Price" />
                <Table.HeaderCell >
                  Artwork<br/>
                  (Click to open)
                </Table.HeaderCell>
                <Table.HeaderCell content="Details" />
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {
                this.state.results.map(generateTrackRow)
              }
            </Table.Body>
          </Table>
        </Card.Content>
      </Card>
    );
  }

}

export default Home;