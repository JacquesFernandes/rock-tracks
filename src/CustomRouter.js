import React from 'react';
import 'semantic-ui-css/semantic.min.css';
import {
  BrowserRouter,
  Route
} from 'react-router-dom';

import CustomHeader from './components/CustomHeader.js';
import Home from './pages/Home.js';
import Details from './pages/Details.js';

class CustomRouter extends React.Component {

  render() {
    return(
      <BrowserRouter>
        <div style={{margin: "0.5em"}} >
          <CustomHeader />
          <Route exact path="/" component={Home} />
          <Route exact path="/track/:trackId" component={Details} />
        </div>
      </BrowserRouter>
    );
  }

}

export default CustomRouter;